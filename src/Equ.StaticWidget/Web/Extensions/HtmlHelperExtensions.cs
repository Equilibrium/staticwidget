﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Equ.StaticWidget.Web.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString RenderStaticType(this HtmlHelper html, string type, object value)
        {
            var httpContext = HttpContext.Current;

            if (httpContext == null)
            {
                throw new NotSupportedException("An HTTP context is required to render the partial view to a string");
            }

            var path = "/App_Plugins/StaticWidget/Views/Types/";
            var viewName = path + type + ".cshtml";

            var controllerName = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();

            var controller = (ControllerBase)ControllerBuilder.Current.GetControllerFactory().CreateController(httpContext.Request.RequestContext, controllerName);

            var controllerContext = new ControllerContext(httpContext.Request.RequestContext, controller);

            var view = ViewEngines.Engines.FindPartialView(controllerContext, viewName).View;

            var sb = new StringBuilder();

            using (var sw = new StringWriter(sb))
            {
                using (var tw = new HtmlTextWriter(sw))
                {
                    view.Render(new ViewContext(controllerContext, view, new ViewDataDictionary(value), new TempDataDictionary(), tw), tw);
                }
            }

            return new HtmlString(sb.ToString());
        }
    }
}