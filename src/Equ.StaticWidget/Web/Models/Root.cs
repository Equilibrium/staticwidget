﻿using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    public class Root
    {
        [JsonProperty("value")]
        public Value Value { get; set; }

        [JsonProperty("editor")]
        public Editor Editor { get; set; }

        [JsonProperty("styles")]
        public object Styles { get; set; }

        [JsonProperty("config")]
        public object Config { get; set; }
    }
}