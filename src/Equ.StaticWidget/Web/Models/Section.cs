﻿using Lucene.Net.Documents;
using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    public partial class Section
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("fields")]
        public Field[] Fields { get; set; }
    }
}