﻿using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    public class Config
    {
        [JsonProperty("__comment__")]
        public string Comment { get; set; }

        [JsonProperty("enablePreview")]
        public bool EnablePreview { get; set; }

        [JsonProperty("largeDialog")]
        public bool LargeDialog { get; set; }

        [JsonProperty("viewPath")]
        public string ViewPath { get; set; }

        [JsonProperty("previewViewPath")]
        public string PreviewViewPath { get; set; }

        [JsonProperty("previewCssFilePath")]
        public string PreviewCssFilePath { get; set; }

        [JsonProperty("previewJsFilePath")]
        public string PreviewJsFilePath { get; set; }
    }
}