﻿using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    /// <summary>
    /// Umbraco Saved Value
    /// </summary>
    public class Value
    {
        [JsonProperty("view")]
        public string View { get; set; }

        [JsonProperty("data")]
        public DataDefinition Data { get; set; }
    }
}