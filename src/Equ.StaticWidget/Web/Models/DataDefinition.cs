﻿using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    public partial class DataDefinition
    {
        [JsonProperty("sections")]
        public Section[] Sections { get; set; }
    }
}