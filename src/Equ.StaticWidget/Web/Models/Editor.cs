﻿using Newtonsoft.Json;

namespace Equ.StaticWidget.Web.Models
{
    public class Editor
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("view")]
        public string View { get; set; }

        [JsonProperty("render")]
        public string Render { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("config")]
        public Config Config { get; set; }
    }
}