﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Equ.StaticWidget.Web.Models;
using Newtonsoft.Json;
using Umbraco.Web.WebApi;
using Log = Serilog.Log;

namespace Equ.StaticWidget.Web.Controllers
{
    [global::Umbraco.Web.Mvc.PluginController("Static")]
    public class StaticWidgetApiController : UmbracoAuthorizedApiController
    {
        private const string StaticViewsPath = "~/Views/Grid/StaticWidget/";

        /// <summary>
        /// route: /umbraco/static/staticWidgetApi/views
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable Views()
        {
            var fullPath = HttpContext.Current.Server.MapPath(StaticViewsPath);

            var staticViews = Directory.GetFileSystemEntries(fullPath);

            var views = new List<string>();

            foreach (var sv in staticViews)
            {
                var attr = File.GetAttributes(sv);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    //grab the children
                    var subViews = Directory.GetFileSystemEntries(sv);
                    views.AddRange(subViews);
                }
                else
                {
                    views.Add(sv);
                }
            }

            return views.Where(x => !x.Contains("Render") && !x.Contains("NotFound") && x.EndsWith(".cshtml", StringComparison.InvariantCultureIgnoreCase)).Select(x => new
            {
                Alias = x.Replace(fullPath, "").Replace(".cshtml", ""),
                Name = x.Replace(fullPath, "").Replace(".cshtml", "")
            });
        }

        [HttpGet]
        public async Task<object> DataDefinition([FromUri] string view)
        {
            var fullPath = HttpContext.Current.Server.MapPath(StaticViewsPath);

            try
            {
                var filePath = fullPath;
                if (view.Contains("\\")) //if in a nice directory grab DataDefinition.json.
                {
                    filePath = Path.Combine(fullPath, view.Substring(0, view.LastIndexOf('\\')), "DataDefinition.json");
                }

                if (File.Exists(filePath))
                {
                    using (var sourceStream = File.Open(filePath, FileMode.Open))
                    {
                        var result = new byte[sourceStream.Length];
                        await sourceStream.ReadAsync(result, 0, (int)sourceStream.Length);

                        //make sure data def valid
                        var rawJson = System.Text.Encoding.UTF8.GetString(result);
                        var def = JsonConvert.DeserializeObject<DataDefinition>(rawJson);

                        return new
                        {
                            Status = "OK",
                            Definition = def
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Warning(ex, "Failed to get static widget definition {0}", view);
#if DEBUG
                return new
                {
                    Status = "Failed",
                    Exception = ex,
                };
#endif
            }

            return new
            {
                Status = "Failed"
            };
        }
    }
}