angular.module('umbraco.resources').factory("Equilibrium.Widgets.Resources.StaticWidgetResources",
    function ($q, $http, umbRequestHelper) {
        return {
            getStaticViews: function () {
                var url = Umbraco.Sys.ServerVariables.umbracoSettings.umbracoPath + "/backoffice/static/staticWidgetapi/views";
                return umbRequestHelper.resourcePromise(
                    $http.get(url),
                    'Failed to retrieve static widget views'
                );
            },
            getDataDefinition: function (view) {
                var url = Umbraco.Sys.ServerVariables.umbracoSettings.umbracoPath + "/backoffice/static/staticWidgetApi/dataDefinition?view=" + view;
                return umbRequestHelper.resourcePromise(
                    $http.get(url),
                    'Failed to retrieve static widget data definition'
                );
            }
        };
    });