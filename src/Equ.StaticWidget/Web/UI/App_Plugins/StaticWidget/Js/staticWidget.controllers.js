angular.module('umbraco').controller('Equilibrium.Widgets.StaticWidget', [
    '$scope',
    '$rootScope',
    '$timeout',
    'editorState',
    'assetsService',
    'Equilibrium.Widgets.Resources.StaticWidgetResources',
    'umbRequestHelper',
    'localizationService',
    'editorService',

    function (
        $scope,
        $rootScope,
        $timeout,
        editorState,
        assetsService,
        staticResources,
        umbRequestHelper,
        localizationService,
        editorService
    ) {
        console.log('static editor current value', $scope.model);
        //$scope.model.value = {};

        var overlayOptions = {
            view: umbRequestHelper.convertVirtualToAbsolutePath(
                '~/App_Plugins/StaticWidget/Views/staticwidget.dialog.html'
            ),
            titles: {
                insertItem: 'Configure widget'
            }
        };

        $scope.showDialog = function () {
            overlayOptions.size = $scope.control.editor.config.largeDialog
                ? null
                : $scope.control.editor.config.overlaySize || 'small';

            overlayOptions.dialogData = {
                value: $scope.control.value
            };

            overlayOptions.close = function () {
                editorService.close();
            };

            overlayOptions.submit = function (newModel) {
                console.log('overlay closed', newModel);
                $scope.model.dialogData = {
                    value: newModel
                };

                $scope.control.value = newModel;

                editorService.close();
            };

            editorService.open(overlayOptions);
        };
    }
]);

angular.module('umbraco').controller('Equilibrium.Widgets.StaticWidgetDialog', [
    '$scope',
    '$interpolate',
    'formHelper',
    'contentResource',
    'blueprintConfig',
    'contentEditingHelper',
    'serverValidationManager',
    'Equilibrium.Widgets.Resources.StaticWidgetResources',

    function (
        $scope,
        $interpolate,
        formHelper,
        contentResource,
        blueprintConfig,
        contentEditingHelper,
        serverValidationManager,
        staticResources
    ) {
        var vm = this;
        vm.submit = submit;
        vm.close = close;
        vm.loading = true;

        //static view selection
        vm.viewName = $scope.model.viewName;
        vm.dataDefinition = $scope.model.dataDefinition;
        vm.showDataDefinition = false;

        if ($scope.model.dialogData.value) {
            vm.viewName = $scope.model.dialogData.value.view;
            vm.dataDefinition = $scope.model.dialogData.value.data;
        }

        vm.toggleDisplayDefinition = function () {
            vm.showDataDefinition = !vm.showDataDefinition;
        };

        vm.updateValue = function () {
            staticResources.getDataDefinition(vm.viewName).then(function (response) {
                if (response.Status === 'OK') {
                    vm.dataDefinition = response.Definition;
                    vm.jsonDataDefinition = JSON.stringify(response.Definition, null, 4);
                }
            });
        };

        function cleanup() {
            if ($scope.model.node && $scope.model.node.id > 0) {
                // delete any temporary blueprints used for validation
                contentResource.deleteBlueprint($scope.model.node.id);

                // set current node id, so subsequent deletes, giving 404 errors is avoided
                $scope.model.node.id = 0;
            }

            //clear server validation messages when this editor is destroyed
            serverValidationManager.clear();
        }

        $scope.$on('$destroy', cleanup);

        function submit() {
            if ($scope.model.submit) {
                console.log('data to collect here', $scope.vm.dataDefinition.sections);
                $scope.model.submit({
                    view: $scope.vm.viewName,
                    data: $scope.vm.dataDefinition
                });
            }
        }
        function close() {
            if ($scope.model.close) {
                $scope.model.close();
            }
        }

        $scope.dialogMode = null;
        $scope.selectedDocType = null;
        $scope.model.node = null;

        var nameExp = !!$scope.model.nameTemplate ? $interpolate($scope.model.nameTemplate) : undefined;

        $scope.model.nameExp = nameExp;

        if ($scope.model.dialogData.docTypeAlias) {
            $scope.dialogMode = 'edit';
            //loadNode();
            console.log('load data?');
        }

        staticResources.getStaticViews().then(function (response) {
            vm.views = response;
            vm.loading = false;
        });

        //set to false once loaded
    }
]);
