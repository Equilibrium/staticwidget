# Static Widget - Developers Guide

### Contents

1. [Introduction](#introduction)
2. [Getting Set Up](#getting-set-up)
   1. [System Requirements](#system-requirements)
3. [Useful Links](#useful-links)

---

### Introduction

---

### Getting Set Up

#### System Requirements

Before you get started, there are a number of things you will need:

1. .NET 4.7.2+
2. Umbraco 8.1.0+
3. The **Equ.StaticWidget** package installed

---

### Configuring Equ Static Widget

### Useful Links

- [Source Code](https://bitbucket.org/Equilibrium/staticwidget/src)
- [Our Umbraco Project Page](http://our.umbraco.org/projects/backoffice-extensions/equ-static-widget)
